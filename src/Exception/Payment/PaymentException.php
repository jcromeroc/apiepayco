<?php


namespace App\Exception\Payment;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PaymentException extends BadRequestHttpException
{
    private const MESSAGE_EMAIL = 'No se ha podido enviar el correo a %s';
    private const MESSAGE_PAY = 'Monto en cartera insuficiente para el pago';
    private const MESSAGE_TOKEN_ERROR = 'Error al crear el token';
    private const MESSAGE_TOKEN_INVALID = "El token no es correcto";

    public static function errorEmail(string $email): self
    {
        throw new self(\sprintf(self::MESSAGE_EMAIL, $email));
    }

    public static function errorPay(): self
    {
        throw new self(\sprintf(self::MESSAGE_PAY));
    }

    public static function tokenError(): self
    {
        throw new self(\sprintf(self::MESSAGE_TOKEN_ERROR));
    }

    public static function tokenInvalid(): self
    {
        throw new self(\sprintf(self::MESSAGE_TOKEN_INVALID));
    }
}