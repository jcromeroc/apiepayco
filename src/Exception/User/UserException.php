<?php


namespace App\Exception\User;



use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserException extends BadRequestHttpException
{
    private const MESSAGE = 'El usuario con correo %s ya existe';

    public static function fromUserEmail(string $email): self
    {
        throw new self(\sprintf(self::MESSAGE, $email));
    }
}