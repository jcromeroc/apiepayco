<?php


namespace App\Exception\Wallet;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class WalletException extends BadRequestHttpException
{
    private const MESSAGE = 'La cartera ya ha sido creada';
    private const MESSAGE_INCREASE = 'Documento o telefono incorrecto';

    public static function errorWalletExist(): self
    {
        throw new self(\sprintf(self::MESSAGE));
    }

    public static function errorWalletData(): self
    {
        throw new self(\sprintf(self::MESSAGE_INCREASE));
    }
}