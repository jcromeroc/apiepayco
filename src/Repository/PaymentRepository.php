<?php


namespace App\Repository;


use App\Entity\Payment;
use App\Entity\User;
use App\Entity\Wallet;

class PaymentRepository extends BaseRepository
{
    protected static function entityClass(): string
    {
        return Payment::class;
    }

    public function save(Payment $payment): void
    {
        $this->saveEntity($payment);
    }

    public function getPayment(int $id): ?Payment
    {
        /** @var Payment $payment */
        $payment = $this->findOneBy(['id' => $id]);
        return  $payment;
    }
}