<?php


namespace App\Repository;


use App\Entity\User;
use App\Entity\Wallet;

class WalletRepository extends BaseRepository
{
    protected static function entityClass(): string
    {
        return Wallet::class;
    }

    public function save(Wallet $wallet): void
    {
        $this->saveEntity($wallet);
    }

    public function getWallet(User $user): ?Wallet
    {
        /** @var Wallet $wallet */
        $wallet = $this->findOneBy(['user' => $user]);
        return  $wallet;
    }
}