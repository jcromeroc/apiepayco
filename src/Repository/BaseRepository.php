<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

abstract class BaseRepository extends ServiceEntityRepository
{

    protected Connection $connection;

    public function __construct(ManagerRegistry $managerRegistry, Connection $connection)
    {
        parent::__construct($managerRegistry, $this->entityClass());
        $this->connection = $connection;
    }

    abstract protected static function entityClass(): string;

    protected function saveEntity($entity): void
    {
        try {
            $this->getEntityManager()->persist($entity);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
        }
    }

    protected function removeEntity($entity): void
    {
        try {
            $this->getEntityManager()->remove($entity);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
        }

    }

    protected function createQueryBuilderCustom(): QueryBuilder
    {
        return $this->getEntityManager()->createQueryBuilder();
    }

    /**
     * @param string $query
     * @param array $params
     * @return array
     * @throws DBALException
     */
    protected function executeFetchQuery(string $query, array $params = []): array
    {
        return $this->connection->executeQuery($query, $params)->fetchAll();
    }

    /**
     * @param string $query
     * @param array $params
     * @throws DBALException
     */
    protected function executeInsertQuery(string $query, array $params = []): void
    {
        $this->connection->executeQuery($query, $params);
    }
}
