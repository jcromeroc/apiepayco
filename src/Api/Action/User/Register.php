<?php

namespace App\Api\Action\User;

use App\Api\Action\RequestTransformer;
use App\Entity\User;
use App\Entity\Wallet;
use App\Exception\User\UserException;
use App\Repository\UserRepository;
use App\Repository\WalletRepository;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class Register
{
    private UserRepository $userRepository;
    private WalletRepository $walletRepository;

    /**
     * @var JWTTokenManagerInterface
     */
    private JWTTokenManagerInterface $JWTTokenManager;

    /**
     * @var EncoderFactoryInterface
     */
    private EncoderFactoryInterface $encoderFactory;

    public function  __construct(UserRepository $userRepository, WalletRepository $walletRepository, JWTTokenManagerInterface $JWTTokenManager, EncoderFactoryInterface $encoderFactory)
    {
        $this->userRepository = $userRepository;
        $this->walletRepository = $walletRepository;
        $this->JWTTokenManager = $JWTTokenManager;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * @Route("/users/register", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function __invoke(Request $request): JsonResponse
    {
        $name = RequestTransformer::getRequiredField($request, 'name');
        $email = RequestTransformer::getRequiredField($request, 'email');
        $password = RequestTransformer::getRequiredField($request, 'password');
        $document = RequestTransformer::getRequiredField($request, 'document');
        $phone = RequestTransformer::getRequiredField($request, 'phone');

        $existingUser = $this->userRepository->findOneByEmail($email);

        if ($existingUser !== null) {
            throw UserException::fromUserEmail($email);
        }

        $user = new User($name, $email, $document, $phone);

        $encoder = $this->encoderFactory->getEncoder($user);

        $user->setPassword($encoder->encodePassword($password, null));

        $this->userRepository->save($user);

        $wallet = new Wallet(0, $user);
        $this->walletRepository->save($wallet);

        $jwt = $this->JWTTokenManager->create($user);

        return new JsonResponse(['token' => $jwt]);
    }

}