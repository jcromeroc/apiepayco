<?php

namespace App\Api\Action\Wallet;


use App\Api\Action\RequestTransformer;
use App\Entity\Wallet;
use App\Exception\Wallet\WalletException;
use App\Repository\UserRepository;
use App\Repository\WalletRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Manage
{
    private WalletRepository $walletRepository;
    private UserRepository $userRepository;


    public function __construct(WalletRepository $walletRepository, UserRepository $userRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/wallet/create", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function createWallet(Request $request)
    {
        $email = RequestTransformer::getRequiredField($request, 'email');
        $existingUser = $this->userRepository->findOneByEmail($email);
        $wallet = $this->walletRepository->getWallet($existingUser);

        if ($wallet !== null) {
            throw WalletException::errorWalletExist();
        } else {
            $wallet = new Wallet(0, $existingUser);
            $this->walletRepository->save($wallet);
            return new JsonResponse(['data' => "Cartera creada"]);
        }
    }

    /**
     * @Route("/wallet/increase", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function increaseWallet(Request $request)
    {
        $email = RequestTransformer::getRequiredField($request, 'email');
        $document = RequestTransformer::getRequiredField($request, 'document');
        $phone = RequestTransformer::getRequiredField($request, 'phone');
        $amount = RequestTransformer::getRequiredField($request, 'amount');

        $existingUser = $this->userRepository->findOneByEmail($email);

        if ($existingUser->getDocument() !== $document || $existingUser->getPhone() !== $phone) {
            throw WalletException::errorWalletData();
        }

        $wallet = $this->walletRepository->getWallet($existingUser);
        $currentAmount = $wallet->getBalance();
        $total = $currentAmount + (float)$amount;
        $wallet->setBalance($total);
        $this->walletRepository->save($wallet);

        return new JsonResponse(['data' => "Cartera recargada", 'balance' => $total]);
    }

    /**
     * @Route("/wallet/balance", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function balanceWallet(Request $request)
    {
        $email = RequestTransformer::getRequiredField($request, 'email');
        $document = RequestTransformer::getRequiredField($request, 'document');
        $phone = RequestTransformer::getRequiredField($request, 'phone');

        $existingUser = $this->userRepository->findOneByEmail($email);

        if ($existingUser->getDocument() !== $document || $existingUser->getPhone() !== $phone) {
            throw WalletException::errorWalletData();
        }

        $wallet = $this->walletRepository->getWallet($existingUser);

        return new JsonResponse(['data' => "Balance de cartera", 'balance' => $wallet->getBalance()]);
    }
}