<?php


namespace App\Api\Action\Payment;


use App\Api\Action\RequestTransformer;
use App\Entity\Payment;
use App\Exception\Payment\PaymentException;
use App\Repository\PaymentRepository;
use App\Repository\UserRepository;
use App\Repository\WalletRepository;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;

class Pay
{
    private PaymentRepository $paymentRepository;
    private WalletRepository $walletRepository;
    private UserRepository $userRepository;

    public function __construct(PaymentRepository $paymentRepository, WalletRepository $walletRepository, UserRepository $userRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->walletRepository = $walletRepository;
        $this->userRepository = $userRepository;
    }

    public function generateToken(): string
    {
        $length = 6;

        try {
            return bin2hex(random_bytes(($length - ($length % 2)) / 2));
        } catch (Exception $e) {
            throw PaymentException::tokenError();
        }
    }

    /**
     * @Route("/payment/pay", methods={"POST"})
     * @param Request $request
     * @param MailerInterface $mailer
     * @return JsonResponse
     * @throws Exception
     */
    public function pay(Request $request, MailerInterface $mailer)
    {
        $email = RequestTransformer::getRequiredField($request, 'email');
        $amount = RequestTransformer::getRequiredField($request, 'amount');

        $existingUser = $this->userRepository->findOneByEmail($email);
        $wallet = $this->walletRepository->getWallet($existingUser);

        if ($wallet->getBalance() < $amount) {
            throw PaymentException::errorPay();
        }

        $token = $this->generateToken();

        $payment = new Payment(new \DateTime('now'), $token, $amount, $existingUser);
        $this->paymentRepository->save($payment);

        $emailToSend = (new TemplatedEmail())
            ->from('jrinjustice777@gmail.com')
            ->to($email)
            ->subject('Token para realizar pago con Epayco')
            ->htmlTemplate('emails/payment.html.twig')
            ->context([
                'userName' => $existingUser->getName(),
                'token' => $token,
            ]);

        try {
            $mailer->send($emailToSend);
        } catch (TransportExceptionInterface $e) {
            throw PaymentException::errorEmail($email);
        }

        return new JsonResponse(['data' => "El código ha sido envíado al correo", 'codigoPago' => $payment->getId()]);
    }

    /**
     * @Route("/payment/confirmation", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function confirmation(Request $request)
    {
        $email = RequestTransformer::getRequiredField($request, 'email');
        $codigoPago = RequestTransformer::getRequiredField($request, 'codigoPago');
        $token = RequestTransformer::getRequiredField($request, 'token');

        $existingUser = $this->userRepository->findOneByEmail($email);
        $payment = $this->paymentRepository->getPayment((int)$codigoPago);

        if ($payment->getToken() !== $token) {
            throw PaymentException::tokenInvalid();
        }

        $wallet = $this->walletRepository->getWallet($existingUser);
        $difference = $wallet->getBalance() - $payment->getAmount();
        $wallet->setBalance($difference);

        $this->walletRepository->save($wallet);

        return new JsonResponse(['data' => "El pago se ha realizado correctamente", 'balance' => $wallet->getBalance()]);
    }



}