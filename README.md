Epayco
===============



Instalación del API de Symfony
===============

1. Bajar el proyecto y en directorio raíz ejecutar el comando: composer install.
2. Se usa la libreria lexik/jwt-authentication-bundle para la autorización de los nodos, para configurarla correr los siguientes comandos:

- $ mkdir -p config/jwt
- $ openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
- $ openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

- Recordar modificar el archivo .env con los valores de la db y la clave usada para generar el key de lexik.

3. Crear la base de datos: php bin/console doctrine:database:create

4. Crear las entidades en la base de datos: php bin/console doctrine:schema:update --force



Uso del API
===============

Se recomienda usar un virtual host en apache para correr el API de Symfony.

A disfrutar!!!
